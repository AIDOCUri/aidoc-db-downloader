import glob
import json
import time
from datetime import datetime

from instance.config import dropbox_set_dir, Dummy_dir
from utils import remove_items_from_list
import os

from DB_interfaces.BlobStorageInterface import BlobStorageInterface
from Storage_interfaces.NASStorageInterface import NASStorageInterface


class ScanDownloader:
    def __init__(self, db_interface, storage_interface):
        self.db_interface = db_interface
        self.storage_interface = storage_interface
        self.temp_path = self.db_interface.download_path

    def create_uid_list(self, uids_to_download=None, uids_to_ignore=None, skip_downloaded_scans=True):
        if uids_to_download is None:
            uids_to_download = self.db_interface.get_list_of_possible_uids()
        if type(uids_to_download) == str:
            uids_to_download = self.load_uid_list(uids_to_download)
        if uids_to_ignore is None:
            uids_to_ignore = []
        if skip_downloaded_scans:
            uids_to_ignore += [i.split('\\')[-1] for i in glob.glob(
                os.path.join(self.storage_interface.save_folder, '*'))]
        if uids_to_ignore is not None:
            uids_to_download = remove_items_from_list(uids_to_download, uids_to_ignore)
        return uids_to_download

    @staticmethod
    def load_uid_list(list_name):
        if list_name[-4:] != 'json':
            list_name += '.json'
        list_path = os.path.join(dropbox_set_dir, list_name)
        with open(list_path, 'r') as f:
            uid_list = json.load(f)['uid_list']
        return uid_list

    def download_scans(self, uids_to_download=None, uids_to_ignore=None, skip_downloaded_scans=True):
        uids_to_download = self.create_uid_list(uids_to_download, uids_to_ignore, skip_downloaded_scans)

        for uid in uids_to_download:
            print('Working on scan %s' % uid)
            t = time.time()
            success = self.db_interface.download_scan_from_uid(uid)
            if success:
                print('Downloaded scan %s in %f seconds' % (uid, time.time() - t))
                t = time.time()
                self.storage_interface.save_scan_from_uid(self.temp_path, uid)
                self.db_interface.clean_temp_files(uid)
                print('Copied scan %s in %f seconds' % (uid, time.time()-t))


if __name__ == '__main__':

    db = BlobStorageInterface(Dummy_dir, 'BRAIN', datetime(2017,11,6))
    storage = NASStorageInterface('test')
    scan_downloader = ScanDownloader(db, storage)

    scan_downloader.download_scans()
