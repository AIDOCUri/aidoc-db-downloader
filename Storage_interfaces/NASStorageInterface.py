import os
import shutil

from Storage_interfaces.StorageInterfaceAbstract import StorageInterfaceAbstract
from instance.config import NASDIR


class NASStorageInterface(StorageInterfaceAbstract):
    def __init__(self, save_folder):
        self.save_folder = os.path.join(NASDIR, save_folder)
        self.create_storage_directory()

    def save_scan_from_uid(self, temp_path, uid):
        temp_scan_dir = os.path.join(temp_path, uid)
        storage_scan_dir = os.path.join(self.save_folder, uid)
        try:
            shutil.copytree(temp_scan_dir, storage_scan_dir)
        except:
            print('problem with shared drives...')


    def create_storage_directory(self):
        os.makedirs(self.save_folder, exist_ok=True)
