from abc import ABCMeta, abstractmethod

class StorageInterfaceAbstract(metaclass=ABCMeta):
    def __init__(self):
        pass

    @abstractmethod
    def save_scan_from_uid(self):
        raise NotImplementedError(
            "StorageInterfaceAbstract Does not implement this method")


