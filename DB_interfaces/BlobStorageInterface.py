import dicom
from azure.storage.blob import BlockBlobService
import os
import io
from datetime import datetime
import pytz
import time

from DB_interfaces.DBInterfaceAbstract import DBInterfaceAbstract
from instance.config import BLOB_STORAGE_ACCOUNT_NAME, BLOB_STORAGE_ACCOUNT_KEY, BLOB_STORAGE_SCAN_CONTAINER_NAME


class BlobStorageInterface(DBInterfaceAbstract):
    def __init__(self, download_path, scan_type=None, from_time=None):
        super().__init__(download_path, scan_type)
        self.block_blob_service = BlockBlobService(account_name=BLOB_STORAGE_ACCOUNT_NAME,
                                                   account_key=BLOB_STORAGE_ACCOUNT_KEY)
        self.container = BLOB_STORAGE_SCAN_CONTAINER_NAME
        self._get_directory_list()
        self.from_time = from_time
        if self.from_time is not None and not self.from_time.tzname():
            utc = pytz.UTC
            self.from_time = utc.localize(self.from_time)


    def get_list_of_possible_uids(self):
        file_names = []
        for i in self.directory_list:
            file_names.append(i.name)
        uids = set([name.split('-')[1].split('/')[0] for name in file_names])
        return self.remove_old_scans(list(uids))

    def remove_old_scans(self, uid_list):
        if self.from_time is None:
            return uid_list
        list_of_good_scans = []
        for uid in uid_list:
            uid_files = self.get_uid_files(uid, num_results=1)
            if self.check_if_new(uid_files):
                list_of_good_scans.append(uid)
        return list_of_good_scans


    def download_scan_from_uid(self, uid):
        self._get_directory_list()
        uid_files = self.get_uid_files(uid)
        if not self.check_scan(uid_files):
            return False
        scan_directory = self.create_scan_directory(uid)
        for dicom_file in uid_files:
            file_path = os.path.join(scan_directory, dicom_file.name.split('/')[-1])
            self.block_blob_service.get_blob_to_path(self.container, dicom_file.name, file_path)
        return True

    def _get_directory_list(self):
        self.directory_list = list(self.block_blob_service.list_blobs(container_name=self.container,
                                                                 delimiter='/'))

    def get_uid_files(self, uid, num_results=None):
        uid_directory_name = None
        for directory_object in self.directory_list:
            if uid in directory_object.name:
                uid_directory_name = directory_object.name

        if uid_directory_name is None:
            return None
        return self.block_blob_service.list_blobs(container_name=self.container,
                                                  prefix=uid_directory_name, num_results=num_results)

    def get_scan_type(self, uid_files):
        example_file = uid_files.items[0]
        dicom_stream = io.BytesIO()
        self.block_blob_service.get_blob_to_stream(self.container, example_file.name, dicom_stream)
        dicom_stream.seek(0)
        try:
            ds = dicom.read_file(dicom_stream, force=True)
            return ds.StudyDescription
        except:
            return None

    def check_if_new(self, uid_files):
        if self.from_time is None:
            return True
        example_file = uid_files.items[0]
        modified_time = example_file.properties.last_modified
        return modified_time > self.from_time
