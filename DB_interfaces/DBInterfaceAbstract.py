import os
import shutil
from abc import ABCMeta, abstractmethod


class DBInterfaceAbstract(metaclass=ABCMeta):
    def __init__(self, download_path, scan_type=None):
        self.download_path = download_path
        self.scan_type = scan_type

    @abstractmethod
    def get_list_of_possible_uids(self):
        raise NotImplementedError(
            "DBInterfaceAbstract Does not implement this method")

    @abstractmethod
    def download_scan_from_uid(self, uid):
        raise NotImplementedError(
            "DBInterfaceAbstract Does not implement this method")

    @abstractmethod
    def get_scan_type(self, dicom_file):
        raise NotImplementedError(
            "DBInterfaceAbstract Does not implement this method")

    def check_scan(self, dicom_files):
        if dicom_files is None:
            print('file not found!')
            return False
        if self.scan_type is None:
            return True
        current_scan_type = self.get_scan_type(dicom_files)
        print(current_scan_type)
        if current_scan_type is None:
            return False
        return self.scan_type in current_scan_type

    def create_scan_directory(self, uid):
        scan_directory = os.path.join(self.download_path, uid)
        os.makedirs(scan_directory, exist_ok=True)
        return scan_directory

    def clean_temp_files(self, uid):
        scan_directory = self.create_scan_directory(uid)
        shutil.rmtree(scan_directory)


