import json
import glob
import os

def remove_items_from_list(main_list, remove_list):
    return [i for i in main_list if i not in remove_list]

def create_positive_uid_list(json_directory, name_of_list):
    json_files = glob.glob(os.path.join(json_directory, '*.json'))
    positive_list = {'uid_list' : []}
    for json_file in json_files:
        with open(json_file, 'r') as f:
            vis = json.load(f)
        if len(vis['finding_dict']) > 0:
            positive_list['uid_list'].append(json_file.split('/')[-1].split('.json')[0])
    with open(os.path.join('/media/hdd1/Dropbox (Aidoc)/TailorMed Data/Sets/DB_scan_lists', name_of_list), 'w') as f:
        json.dump(positive_list, f)
    print('found %d positive scans' % len(positive_list['uid_list']))

if __name__ == '__main__':
    create_positive_uid_list('/media/hdd1/uri_goren/scans_from_cloud/cedars', 'first_cedars_positive_batch')
